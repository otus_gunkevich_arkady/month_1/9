﻿using game_Library.Menu;

namespace game_Library
{
    public interface IGame
    {
        string Title();
        ConsoleColor TitleColor();
        List<string> Rules();
        void Start();
        IMenuService Setting();
    }
}
