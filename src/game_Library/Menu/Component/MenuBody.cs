﻿namespace game_Library.Menu.Component
{
    public class MenuBody
    {
        private List<MenuButton> _buttons { get; set; }
        private List<string> _text { get; set; }
        public MenuBody(List<MenuButton> buttons, List<string> text)
        {
            _buttons = buttons;
            if (!buttons.Any(s => s.IsSelect))
                buttons.First().IsSelect = true;
            _text = text;
        }
        public void Write()
        {
            Console.Clear();
            _buttons.ForEach(s =>
            {
                if (s.IsSelect) Console.BackgroundColor = s.ColorSelect;
                Console.WriteLine($"{s.Text}");
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _text.ForEach(s => Console.WriteLine(s));
        }
        public void ExecuteButton() => _buttons.SingleOrDefault(s => s.IsSelect).Execute();
        public void UpSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex > 0)
            {
                _buttons[_changeIndex - 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
        public void DownSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex < _buttons.Count() - 1)
            {
                _buttons[_changeIndex + 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
    }
}
