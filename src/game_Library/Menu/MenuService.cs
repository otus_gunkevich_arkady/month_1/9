﻿using game_Library.Menu.Component;

namespace game_Library.Menu
{
    public class MenuService : IMenuService
    {
        protected MenuBody _menu;
        protected bool _exit = false;
        public virtual void Enter() => _menu.ExecuteButton();
        public virtual void Up() => _menu.UpSelect();
        public virtual void Down() => _menu.DownSelect();
        public virtual void Left() { }
        public virtual void Right() { }
        public virtual void Write() => _menu.Write();
        public virtual bool Exit() { return _exit; }
    }
}

