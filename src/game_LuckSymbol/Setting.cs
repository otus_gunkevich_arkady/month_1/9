﻿namespace game_LuckSymbol
{
    public class Setting
    {
        private Dictionary<string, string> _listWord;
        private Difficult _difficult;
        private enum Difficult
        {
            Easy,
            Normal,
            Expert
        }
        public Setting()
        {
            _difficult = Difficult.Easy;
        }
        public List<string> MenuFooter() => new List<string>() {
            $"Выбранная сложность {_difficult}",
            $"для изменения сложности используйте стрелки влево/вправо"
        };
        public void ChangeDifficult(bool side)
        {
            if (side && _difficult != Difficult.Expert)
                _difficult += 1;
            else if (!side && _difficult != Difficult.Easy)
                _difficult -= 1;
        }
        public Dictionary<string, string> ListWord()
        {
            if (_listWord is null) LoadData();
            return _listWord;
        }
        public void LoadData()
        {
            _listWord = new Dictionary<string, string>();
            using (var reader = new StreamReader(@"Source\WordRandom.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(',');
                    switch (_difficult)
                    {
                        default:
                        case Difficult.Easy:
                            {
                                if (line[0].Length < 6 && !_listWord.ContainsKey(line[0]))
                                    _listWord.Add(line[0], line[1]);
                                break;
                            }
                        case Difficult.Normal:
                            {
                                if (line[0].Length >= 6 && line[0].Length < 8 && !_listWord.ContainsKey(line[0])) _listWord.Add(line[0], line[1]);
                                break;
                            }
                        case Difficult.Expert:
                            {
                                if (line[0].Length >= 8 && !_listWord.ContainsKey(line[0])) _listWord.Add(line[0], line[1]);
                                break;
                            }
                    }
                }
            }
        }
    }
}
