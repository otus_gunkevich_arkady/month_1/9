﻿using game_Library;
using game_Library.Menu;

namespace game_LuckSymbol
{
    public class Game : IGame
    {
        Setting _setting;
        private bool _isGame;
        public Game()
        {
            _isGame = false;
            _setting = new Setting();
        }
        public ConsoleColor TitleColor() => ConsoleColor.Blue;
        public string Title() => "Игра: Угадай символ!";
        public IMenuService Setting() => new SettingMenu(_setting);
        public List<string> Rules() => new List<string>() {
            $"Игра будет показывать случайное слово без символов"
        };
        public void Start()
        {
            _isGame = true;
            Random _rnd = new Random();
            Console.Clear();
            Console.WriteLine("Введите -1: для того что бы выйти");
            Console.WriteLine("Введите  0: для того что бы начать заного");
            Console.WriteLine("Введите  1: для того что бы получить подсказку");
            int _try = 0;
            string _input = string.Empty;
            var _objectAnswer = _setting.ListWord().ElementAt(_rnd.Next(0, _setting.ListWord().Count()));
            char _charAnswer = _objectAnswer.Key[_rnd.Next(0, _objectAnswer.Key.Length - 1)];
            Console.WriteLine($"Загаданное: {_objectAnswer.Key.Replace(_charAnswer, '*')}");
            while (_input != "-1")
            {
                if (_isGame) { Console.Write("Введите спрятанную букву:"); }
                else
                {
                    Console.WriteLine("Введите -1: для того что бы выйти");
                    Console.WriteLine("Введите  0: для того что бы начать заного");
                    Console.Write("Игра окончена, что дальше:"); 
                }
                _input = Console.ReadLine();
                if (_input is null) { Console.WriteLine("Неверный ввод"); }
                else
                {
                    _try += 1;
                    if (_input == "0") { Start(); return; }
                    else if (_input == "1") { Console.WriteLine($"Подсказка: {_objectAnswer.Value}"); }
                    else if (_input == _charAnswer.ToString()) { Console.WriteLine($"Вы отгадали, это слово {_objectAnswer.Key}"); _isGame = false; }
                    else if (_input != _charAnswer.ToString()) { Console.WriteLine("Вы не отгадали."); }
                }
            }
            return;
        }
    }
}
