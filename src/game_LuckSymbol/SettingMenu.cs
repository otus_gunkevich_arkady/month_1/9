﻿using game_Library.Menu;
using game_Library.Menu.Component;

namespace game_LuckSymbol
{
    public class SettingMenu : MenuService
    {
        private Setting _setting;
        public SettingMenu(Setting setting)
        {
            _setting = setting;
            LoadMenu();
        }
        public override void Left() { _setting.ChangeDifficult(false); LoadMenu(); }
        public override void Right() { _setting.ChangeDifficult(true); LoadMenu(); }
        private void LoadMenu()
        {
            List<MenuButton> _buttons = new List<MenuButton>();
            List<string> _text = new List<string>();

            _buttons.Add(new MenuButton("[*] Назад (Сохранить)", () => { _setting.LoadData(); this._exit = true; }));

            _text = _setting.MenuFooter();

            _menu = new MenuBody(_buttons, _text);
        }
    }
}
