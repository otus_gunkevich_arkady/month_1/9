﻿using DotnetDev.Homework._9;

internal class Program
{
    private static void Main(string[] args) => new Games().Start();
}