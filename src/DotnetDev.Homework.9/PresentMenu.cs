﻿using game_Library.Menu;

namespace DotnetDev.Homework._9
{
    internal class PresentMenu
    {
        private IMenuService _menuService;
        public PresentMenu(IMenuService menuService) { _menuService = menuService; }
        public void Present()
        {
            while (!_menuService.Exit())
            {
                _menuService.Write();

                var _key = Console.ReadKey();
                switch (_key.Key)
                {
                    case ConsoleKey.UpArrow: { _menuService.Up(); break; }
                    case ConsoleKey.DownArrow: { _menuService.Down(); break; }
                    case ConsoleKey.LeftArrow: { _menuService.Left(); break; }
                    case ConsoleKey.RightArrow: { _menuService.Right(); break; }
                    case ConsoleKey.Enter: { _menuService.Enter(); break; }
                    default: { break; }
                }
            }
        }
    }
}
