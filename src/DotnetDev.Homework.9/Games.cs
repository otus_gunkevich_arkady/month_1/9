﻿using game_Library;

namespace DotnetDev.Homework._9
{
    internal class Games
    {
        private List<IGame> _gameList;
        public Games()
        {
            _gameList = new List<IGame>
                {
                    new game_LuckNumber.Game(),
                    new game_LuckSymbol.Game()
                };
        }
        public void Start() => new PresentMenu(new ProgrammMenu(_gameList)).Present();
    }
}
