﻿using game_Library;
using game_Library.Menu;
using game_Library.Menu.Component;

namespace DotnetDev.Homework._9
{
    internal class ProgrammMenu : MenuService
    {
        private Stack<MenuPage> _menuStack;
        private enum MenuPage
        {
            Default,
            Game,
            GameRules
        }
        List<IGame> _games;
        IGame _selectGame;
        public ProgrammMenu(List<IGame> games)
        {
            _games = games;
            _menuStack = new Stack<MenuPage>();
            PushMenu(MenuPage.Default);
        }
        private void LoadMenu()
        {
            var _page = _menuStack.Peek();

            List<MenuButton> _buttons = new List<MenuButton>();
            List<string> _text = new List<string>();

            switch (_page)
            {
                case MenuPage.Default:
                    {
                        _games.ForEach(s => _buttons.Add(new MenuButton(s.Title(), () => { _selectGame = s; PushMenu(MenuPage.Game); }, s.TitleColor())));

                        _text.Add("");
                        _text.Add("");
                        _text.Add("Гункевич Аркадий Игоревич | OTUS");
                        _text.Add("ДЗ: Демонстрация SOLID принципов");
                        break;
                    }
                case MenuPage.Game:
                    {
                        _text.Add(_selectGame.Title());
                        _buttons.Add(new MenuButton("Начать", () => _selectGame.Start()));
                        _buttons.Add(new MenuButton("Правила", () => PushMenu(MenuPage.GameRules)));
                        _buttons.Add(new MenuButton("Настройки", () => new PresentMenu(_selectGame.Setting()).Present()));
                        break;
                    }
                case MenuPage.GameRules:
                    {
                        _text = _selectGame.Rules();
                        break;
                    }
            }

            if (_page != MenuPage.Default) _buttons.Add(new MenuButton("[*] Назад", () => DownMenu()));
            _buttons.Add(new MenuButton("[*] Закрыть приложение", () => this._exit = true));
            _menu = new MenuBody(_buttons, _text);
        }
        private void PushMenu(MenuPage menuPage)
        {
            if (!_menuStack.Contains(menuPage))
                _menuStack.Push(menuPage);
            LoadMenu();
        }
        private void DownMenu()
        {
            _menuStack.Pop();
            LoadMenu();
        }
    }
}
