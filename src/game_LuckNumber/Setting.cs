﻿namespace game_LuckNumber
{
    public class Setting
    {
        public int GetValue => _value;
        private int _value;
        private int _min;
        private int _max;
        public Setting()
        {
            _value = 20;
            _min = 5;
            _max = 100;
        }
        public List<string> MenuFooter() => new List<string>() { 
            $"Виберете число, в диапазоне которого",
            $"игра будет загадывать.",
            $"Текущее значение {_value}",
            $"Минимальное значение {_min}",
            $"Максимальное значение {_max}",
        };
        public void ChangeValue(int value)
        {
            _value += value;
            _value = _value > _max ? _max : _value;
            _value = _value < _min ? _min : _value;
        }
    }
}
