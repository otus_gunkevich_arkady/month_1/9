﻿using game_Library;
using game_Library.Menu;

namespace game_LuckNumber
{
    public class Game : IGame
    {
        Setting _setting;
        private bool _isGame;
        public Game()
        {
            _isGame = false;
            _setting = new Setting();
        }
        public ConsoleColor TitleColor() => ConsoleColor.Red;
        public string Title() => "Игра: Угадай число!";
        public IMenuService Setting() => new SettingMenu(_setting);
        public List<string> Rules() => new List<string>() {
            $"Игра будет загадывать число в диапазоне от {1} до {_setting.GetValue}",
            $"Ваша задача как можно быстрее опознать загаданное число.",
            $"Если вы неверное укажите число игра подскажет, больше или меньше.",
            $"Чем меньше попыток, тем лучше."
        };
        public void Start()
        {
            _isGame = true;
            Random _rnd = new Random();
            Console.Clear();
            Console.WriteLine("Введите -1: для того что бы выйти");
            Console.WriteLine("Введите  0: для того что бы начать заного");
            Console.WriteLine($"Случайная цифра в промежутке от {1} до {_setting.GetValue} загадана");
            int _input = 0;
            int _try = 0;
            int _answer = _rnd.Next(1, _setting.GetValue);
            while (_input != -1)
            {
                if (_isGame) { Console.Write($"Введите какое нибудуть число ({1} <> {_setting.GetValue}):"); }
                else
                {
                    Console.WriteLine("Введите -1: для того что бы выйти");
                    Console.WriteLine("Введите  0: для того что бы начать заного");
                    Console.Write("Игра окончена, что дальше:"); 
                }
                var _successInput = int.TryParse(Console.ReadLine(), out _input);
                if (_successInput)
                {
                    _try += 1;
                    if (_input == 0) { Start(); return; }
                    if (_answer == _input) { Console.WriteLine($"Вы отгадали число. Попыток {_try}"); _isGame = false; }
                    else if (_answer < _input) { Console.WriteLine("Загаданное число меньше."); }
                    else if (_answer > _input) { Console.WriteLine("Загаданное число больше."); }
                }
                else { Console.WriteLine("Неверный ввод"); }
            }
            return;
        }
    }
}
