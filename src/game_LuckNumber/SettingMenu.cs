﻿using game_Library.Menu;
using game_Library.Menu.Component;

namespace game_LuckNumber
{
    public class SettingMenu : MenuService
    {
        private Setting _setting;
        public SettingMenu(Setting setting)
        {
            _setting = setting;
            LoadMenu();
        }
        private void LoadMenu()
        {
            List<MenuButton> _buttons = new List<MenuButton>();
            List<string> _text = new List<string>();

            _buttons.Add(new MenuButton("+10", () => { _setting.ChangeValue(10); LoadMenu(); }, ConsoleColor.Green));
            _buttons.Add(new MenuButton("+1", () => { _setting.ChangeValue(1); LoadMenu(); }, ConsoleColor.Green));
            _buttons.Add(new MenuButton("-1", () => { _setting.ChangeValue(-1); LoadMenu(); }, ConsoleColor.Red));
            _buttons.Add(new MenuButton("-10", () => { _setting.ChangeValue(-10); LoadMenu(); }, ConsoleColor.Red));
            _buttons.Add(new MenuButton("[*] Назад (Сохранить)", () => this._exit = true));

            _text = _setting.MenuFooter();

            _menu = new MenuBody(_buttons, _text);
        }
    }
}
